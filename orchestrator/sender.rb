#!/usr/bin/ruby

##############################################################################
# Environment Configuration
##############################################################################
ONE_LOCATION = ENV['ONE_LOCATION']

if !ONE_LOCATION
    RUBY_LIB_LOCATION = '/usr/lib/one/ruby' if !defined?(RUBY_LIB_LOCATION)
    GEMS_LOCATION     = '/usr/share/one/gems' if !defined?(GEMS_LOCATION)
else
    RUBY_LIB_LOCATION = ONE_LOCATION + '/lib/ruby' if !defined?(RUBY_LIB_LOCATION)
    GEMS_LOCATION     = ONE_LOCATION + '/share/gems' if !defined?(GEMS_LOCATION)
end

if File.directory?(GEMS_LOCATION)
    Gem.use_paths(GEMS_LOCATION)
end

$LOAD_PATH << RUBY_LIB_LOCATION

$CONFIG_FILE = "/etc/hypercx-dr/dr.conf.yaml"

##############################################################################
# Required libraries
##############################################################################
require 'opennebula'
require 'nokogiri'
require '/usr/lib/one/ruby/CommandManager.rb'
#require 'pry'
require 'optparse'

include OpenNebula

class Monitor
    attr_accessor :xml

    def initialize(element, prefix)
        @prefix = prefix
        t1 = Time.now
        if element.kind_of? OpenNebula::VirtualMachinePool
          rc = element.info_all_extended
        else
          rc = element.info
        end
        response_time = (Time.now - t1) * 1000.0
        if OpenNebula.is_error?(rc)
             exit (-1)
        end
        @object = element
        @xml = Nokogiri::XML(element.to_xml)
	    self.xml = @xml
	    @entries = Array.new

        send_to_zabbix("response_time_#{@prefix}", response_time)
    end

    ## Gets amount of items in pool. Ex: Amount of VMs in VM_POOL
    def get_amount
        counter = 0
        @xml.xpath("#{@prefix}_POOL/#{@prefix}").each do
            counter = counter + 1
        end
        send_to_zabbix("#{@prefix}.total", counter)
    end

    ### Gets total amount of ARs, IPs and used IPs
    def get_ip_amount
        total_ar = 0
        total_ips = 0
        used_ips = 0
        @xml.xpath("#{@prefix}_POOL/#{@prefix}/AR_POOL/AR").each do |ar|
            total_ar = total_ar + 1
            ar_xml = Nokogiri::XML(ar.to_xml)
            total_ips = total_ips + ar_xml.xpath("AR/SIZE").text.to_i
            used_ips = used_ips + (ar_xml.xpath("AR/ALLOCATED").text.split(' ').size / 2)
        end
        send_to_zabbix("AR.total", total_ar)
        send_to_zabbix("IP.total", total_ips)
        send_to_zabbix("IP.used", used_ips)
    end

    #Discovery of all elements belonging to a pool
    def retrieve_element(xpath, key, subkey)
        avoid_elements=["serveradmin"]
        array = []
	    @entries.clear
        if subkey == "#ZONE"
            xpath = xpath.split('/').first(2).join('/')
            zone_servers = @xml.xpath("#{xpath}/SERVER_POOL/SERVER/ID")
            #Only supports three servers HA zone at the moment
            if zone_servers.length == 3
                xpath = "#{xpath}/NAME"
            else
                return
            end
        end
        @xml.xpath(xpath).each do |resource|
            if !avoid_elements.include?(resource.text)
                array.push({"{#NAME}"=>resource.text.gsub(' ','_')})
	            @entries.push(resource.text)
            end
        end
        send_to_zabbix(key, "'{\"data\":#{array.to_s.gsub('=>',':')}}'")
    end

    ### Element_info for datastores
    def datastores_info(prefix, elements)
        free_mb = Hash.new
        total_mb = Hash.new
        elements.each do |element|
            #Returns parameters for a single element
	        names = Array.new
	        values = Array.new
	        mixed = Array.new
	        @xml.xpath("#{prefix}_POOL/#{@prefix}/NAME").each do |name|
	        	names.push(name.text)
	        end
	        @xml.xpath("#{prefix}_POOL/#{@prefix}/#{element}").each do |value|
	        	values.push(value.text)
	        end

	        mixed = names.zip(values)

            if prefix == "DATASTORE"
                case element
                    when "FREE_MB"
                        mixed.each do |array|
                            free_mb[array[0]] = array[1]
                        end
                    when "TOTAL_MB"
                        mixed.each do |array|
                            total_mb[array[0]] = array[1]
                        end
                end
            end

	        mixed.each do |array|
                send_to_zabbix("#{element}[#{array[0]}]", array[1])
	        end
        end
            
        if prefix == "DATASTORE"
            total_mb.keys.each do |datastore|
                send_to_zabbix("FREE_MB_PCT[#{datastore}]", (free_mb[datastore].to_f / total_mb[datastore].to_f)*100 )
            end
        end
    end

    def vms_info(prefix, elements)
        monitoring_elements = ['CPU', 'MEMORY', 'NETTX', 'NETRX', 'DISKRDBYTES', 'DISKWRBYTES', 'DISKRDIOPS', 'DISKWRIOPS']
        template_elements = ['CPU', 'MEMORY', 'DISK/SIZE']
        datastore_allocated_space = Hash.new
        @object.each do |vm|
            vm_mon_info = vm.monitoring( monitoring_elements )
            vm_xml = Nokogiri::XML(vm.to_xml)
            vm_datastore_id = vm_xml.xpath("VM/HISTORY_RECORDS/HISTORY/DS_ID").text.to_i
            puts "ds_id = #{vm_datastore_id}"
            monitoring_elements.each do |element|
                    if vm_mon_info.is_a?(Hash) and !vm_mon_info[element].empty?
                        send_to_zabbix("MONITORING.#{element}[#{vm.name.gsub(' ','_')}]", vm_mon_info[element].last[1])
                    end
            end
            template_elements.each do |el|
                    total=0
                    vm_xml.xpath("VM/TEMPLATE/#{el}").each do |value|
                        total = total + value.text.to_f
                    end
                    if el == "DISK/SIZE"
                      if datastore_allocated_space[vm_datastore_id]
                        datastore_allocated_space[vm_datastore_id] = datastore_allocated_space[vm_datastore_id] + total
                      else
                        datastore_allocated_space[vm_datastore_id] = total
                      end
                    end
                    send_to_zabbix("TEMPLATE.#{el.gsub('/','.')}[#{vm.name.gsub(' ','_')}]", total)
            end
        end
        #### This is a shame. Keys use datastore name, but VM HISTORY only gives datastore ID, so I need 
        #### extra resources just to find out the name. Also, this info should be sent with the "datastore"
        #### resource, but it is CONSIDERABLY more efficient to send it here
        client_temp = Client.new
        datastore_allocated_space.keys.each do |ds_id|
          datastore = Datastore.new_with_id(ds_id, client_temp)
          datastore.info
          send_to_zabbix("ALLOCATED_SPACE[#{datastore.to_hash['DATASTORE']['NAME']}]", datastore_allocated_space[ds_id])
        end
    end

    def images_info(prefix, elements)
        images_used_space = Hash.new
        @object.each do |image|
            image_hash = image.to_hash
            elements.each do |element|
                 if image_hash.is_a?(Hash) and !image_hash['IMAGE'][element].empty? 
                     if images_used_space.key(image_hash['IMAGE']['DATASTORE_ID'].to_i) 
                       images_used_space[image_hash['IMAGE']['DATASTORE_ID'].to_i] =\
                         images_used_space[image_hash['IMAGE']['DATASTORE_ID'].to_i] + image_hash['IMAGE']['SIZE'].to_i
                     else
                       images_used_space[image_hash['IMAGE']['DATASTORE_ID'].to_i] = image_hash['IMAGE']['SIZE'].to_i
                     end
                     send_to_zabbix("MONITORING.#{element}[#{image.name.gsub(' ','_')}]", image_hash['IMAGE'][element])
                 end
            end
        end

        client_temp = Client.new
        datastores = DatastorePool.new(client_temp)
        datastores.info
        images_used_space.keys.each do |image_ds_id|
          image_ds = Datastore.new_with_id(image_ds_id, client_temp)
          image_ds.info
          image_ds_total_space = image_ds.to_hash['DATASTORE']['TOTAL_MB']
          datastores.each do |datastore|
            datastore_hash = datastore.to_hash
            if datastore_hash['DATASTORE']['TYPE'] == "1" and datastore_hash['DATASTORE']['TOTAL_MB'] == image_ds_total_space
              available_space_for_vms = datastore_hash['DATASTORE']['TOTAL_MB'].to_i - images_used_space[image_ds_id].to_i
              send_to_zabbix("AVAILABLE_VM_SPACE[#{datastore_hash['DATASTORE']['NAME']}]", available_space_for_vms)
            end
          end
        end
    end

    ### Extra info (element info) for users
    def user_belongings(xml, prefix)
	      names = Array.new
	      @xml.xpath("#{@prefix}_POOL/#{@prefix}/NAME").each do |name|
	      	names.push(name.text)
	      end
	      names.each do |user_name|
	      	counter = 0
	      	xml.xpath("#{prefix}_POOL/#{prefix}").each do |element|
	      		if element.xpath("UNAME").text == user_name
	      			counter = counter + 1
	      		end
	      	end
              send_to_zabbix("#{prefix}[#{user_name}]", counter)
	      end
    end

    ### Get amount of items from a pool in specific hardcoded states
    def get_amount_per_state(xpath,key)
    #Returns the total amount of elements and amount of elements per state for a specified pool
        iden = {:total => 0, :id_0 => 0, :id_1 => 0, :id_2 => 0, :id_3 => 0, :id_4 => 0, :id_5 => 0, :id_6 => 0, :id_7 => 0, :id_8 => 0, :id_9 => 0, :id_10 => 0, :id_11 => 0, :id_12 => 0, :id_16 => 0, :id_36 => 0, :id_37 => 0, :id_38 => 0, :id_39 => 0, :id_40 => 0 }
        @xml.xpath(xpath).each do |state|
                iden[:total] = iden[:total] + 1
                state_formatted = "id_#{state.text}".to_sym
                if iden[state_formatted]
                    iden[state_formatted] = iden[state_formatted] + 1
                end
        end
        iden.each_key do |elem|
            send_to_zabbix("#{key}.#{elem.to_s}", iden[elem.to_sym])
        end
    end
   
    def get_resource(xpath, key)
        resource = 0
        @xml.xpath("#{xpath}").each do |value|
        	resource = resource + value.text.to_i
        end
        send_to_zabbix(key, resource)
    end

    def get_unique_resource_per_cluster(xpath, resource_path, key)
        resource = 0
        @xml.xpath("#{xpath}").each do |value|
            if value.xpath("CLUSTER").text == "KVM"
                    resource = resource + value.xpath(resource_path).text.to_i
            end
        end
        send_to_zabbix(key, resource)
    end

    ### Extra info (element info) for zones
    def get_zone_health(xpath)
        @xml.xpath("#{xpath}/NAME").each do |resource|
            counter = 0
            zone_name = resource.text
            @xml.xpath("#{xpath}/SERVER_POOL/SERVER/ENDPOINT").each do |element|
                counter = counter + 1
                endpoint = element.text
                remote_client = OpenNebula::Client.new(nil, endpoint, {:timeout => 5})
                xml = Nokogiri::XML(remote_client.call("zone.raftstatus"))
                index = xml.xpath("RAFT/LOG_INDEX").text
                commit = xml.xpath("RAFT/COMMIT").text
                send_to_zabbix("ZONE[#{zone_name},index#{counter}]", index)
                send_to_zabbix("ZONE[#{zone_name},commit#{counter}]", commit)
                send_to_zabbix("ZONE[#{zone_name},difference#{counter}]", index.to_i - commit.to_i)
            end
                send_to_zabbix("zone_total_servers", counter)
        end
    end
     
    def send_to_zabbix(key, value)
        ZABBIX_SERVER.split(',').each do |server|
            puts "zabbix_sender -z #{server} -p #{ZABBIX_PORT} -s #{HOSTNAME} -k #{key} -o #{value}" if VERBOSE
            response = LocalCommand.run("zabbix_sender -z #{server} -p #{ZABBIX_PORT} -s #{HOSTNAME} -k #{key} -o #{value}")
            puts response.stdout if VERBOSE
        end
    end
end

##########################################################################################
################################BEGIN#####################################################
##########################################################################################

options = {}
ARGV.options do |opts|
    script_name = File.basename($PROGRAM_NAME)
    opts.banner = 'Collect metrics from HyperCX'
    opts.define_head("Usage: #{script_name} [OPTIONS]",
                     'Example', "#{script_name} -s 172.17.52.5 -o Cluster_Name -r vm -a get_amount_per_state")
    opts.separator('')
    opts.on('-r', '--resource STR', String,
            'Resource to monitor. Available options: vm, host, cluster, image, group, vnet, datastore, user, ha_zone.') {|v| options[:resource] = v }
    opts.on('-a', '--action STR', String,
            "Monitoring action. Available options: get_total_amount, get_amount_per_state, discovery, element_info, all.\n"\
            "get_total_amount --> Total amount of elements in a pool \n"\
            "get_amount_per_state --> Return the amount of elements in each of the hardcoded states \n"\
            "discovery --> Populates a discovery rule \n"\
            "element_info --> Returns specific info regarding each element in a pool. Used to fill out items created through discovery rules \n"\
            "all --> Will send ALL supported items (all of the above) for the resource while doing a single call to request the entire pool") {|v| options[:action] = v }
    opts.on('-s', '--zabbix_server STR', String,
            'Zabbix server.') {|v| options[:server] = v }
    opts.on('-p', '--zabbix_port STR', String,
            'Zabbix port. Default: 10051.') {|v| options[:port] = v }
    opts.on('-o', '--hostname STR', String,
            'Hostname specified on zabbix for this host') {|v| options[:hostname] = v }
    opts.on('-v', '--verbose') { options[:verbose] = true }
    opts.separator('')
    opts.on_tail('-h', '--help', 'Show this help message.') do
        puts(opts)
        exit(0)
    end
    opts.parse!
end

options[:port] = "10051" unless options.key?(:port)
if options[:verbose]
    VERBOSE = true 
else
    VERBOSE = false
end

begin
    raise "Unknown option(s) #{ARGV.join(', ')}" if ARGV.any?
    raise "No Zabbix server defined" unless options.key?(:server)
    raise 'No hostname defined' unless options.key?(:hostname)
    raise 'No resource defined' unless options.key?(:resource)
    raise 'No action defined' unless options.key?(:action)
rescue Exception => ex
    puts "#{ex.message}. Please use -h or --h for usage."
    exit(1)
end

client = Client.new
ZABBIX_SERVER = options[:server]
ZABBIX_PORT = options[:port]
HOSTNAME = options[:hostname]

case options[:resource]
when "vm"
  pool = VirtualMachinePool.new(client, -2)
  xml_path = "VM_POOL/VM"
  key = "VM"
  discovery_key = "one.vm.name"
  lcm_key = "LCM_VM"
  elements = ['MONITORING/CPU', 'MONITORING/MEMORY', 'MONITORING/NETRX', 'MONITORING/NETTX', 'TEMPLATE/DISK/SIZE', 'TEMPLATE/CPU', 'TEMPLATE/MEMORY']
when "host"
  pool = HostPool.new(client)
  xml_path = "HOST_POOL/HOST"
  discovery_key = "one.host.name"
  key = "HOST"
when "cluster"
  pool = ClusterPool.new(client)
  key = "CLUSTER"
when "group"
  pool = GroupPool.new(client)
  xml_path = ""
  key = "GROUP"
when "vnet"
  pool = VirtualNetworkPool.new(client, -1)
  xml_path = ""
  key = "VNET"
when "image"
  pool = ImagePool.new(client, -2)
  xml_path = "IMAGE_POOL/IMAGE"
  discovery_key = "one.image.name"
  elements = ['SIZE']
  key = "IMAGE"
when "datastore"
  pool = DatastorePool.new(client)
  xml_path = "DATASTORE_POOL/DATASTORE"
  key = "DATASTORE"
  discovery_key = "one.datastore.name"
  elements = ['FREE_MB', 'TOTAL_MB', 'USED_MB']
when "user"
  pool = UserPool.new(client)
  xml_path = "USER_POOL/USER"
  key = "USER"
  discovery_key = "one.user.name"
  elements = ['FREE_MB', 'TOTAL_MB', 'USED_MB']
when "ha_zone"
  pool = ZonePool.new(client)
  xml_path = "ZONE_POOL/ZONE"
  discovery_key = "one.zone.name"
  key = "ZONE"
else
  STDERR.puts "ERROR. Element not recognized."
end

monitor = Monitor.new(pool, key)

case options[:action]
when "get_amount_per_state"
  monitor.get_amount_per_state("#{xml_path}/STATE",key)
when "get_amount_per_lcm_state"
  monitor.get_amount_per_state("#{xml_path}/LCM_STATE",lcm_key)
when "get_total_amount"
  monitor.get_amount
  if options[:resource] == "vnet"
    monitor.get_ip_amount
  end
when "discovery"
  monitor.retrieve_element("#{xml_path}/NAME", discovery_key, "##{key}")
when "element_info"
  case options[:resource]
  when "datastore"
    monitor.datastores_info(key, elements)
  when "vm"
    monitor.vms_info(key, elements)
  when "image"
    monitor.images_info(key, elements)
  when "user"
    vmpool = VirtualMachinePool.new(client, -2)
    vmpool_info = Monitor.new(vmpool, "VM")
    imagepool = ImagePool.new(client, -2)
    imagepool_info = Monitor.new(imagepool, "IMAGE")
    monitor.user_belongings(vmpool_info.xml, "VM")
    monitor.user_belongings(imagepool_info.xml, "IMAGE")
  when "ha_zone"
    monitor.get_zone_health(xml_path)
  when "host"
    monitor.get_resource("#{xml_path}/HOST_SHARE/MAX_CPU", "cluster.total.cpu")
    monitor.get_resource("#{xml_path}/HOST_SHARE/CPU_USAGE", "cluster.allocated.cpu")
    monitor.get_resource("#{xml_path}/HOST_SHARE/USED_CPU", "cluster.used.cpu")
    monitor.get_resource("#{xml_path}/HOST_SHARE/MAX_MEM", "cluster.total.ram")
    monitor.get_resource("#{xml_path}/HOST_SHARE/MEM_USAGE", "cluster.allocated.ram")
    monitor.get_resource("#{xml_path}/HOST_SHARE/USED_MEM", "cluster.used.ram")
    monitor.get_unique_resource_per_cluster(xml_path, "HOST_SHARE/TOTAL_CPU", "cluster.real.cpu")
    monitor.get_unique_resource_per_cluster(xml_path, "HOST_SHARE/TOTAL_MEM", "cluster.real.ram")
  else
    STDERR.puts "Element info not supported for resource #{options[:resource]}"
  end
when "all"
  case options[:resource]
  when "datastore"
    ## Discovery 
    monitor.retrieve_element("#{xml_path}/NAME", discovery_key, "##{key}")
    ## element_info
    monitor.datastores_info(key, elements)
  when "user"
    ## Get total amount
    monitor.get_amount
    ## Discovery 
    monitor.retrieve_element("#{xml_path}/NAME", discovery_key, "##{key}")
    ##element_info
    vmpool = VirtualMachinePool.new(client, -2)
    vmpool_info = Monitor.new(vmpool, "VM")
    imagepool = ImagePool.new(client, -2)
    imagepool_info = Monitor.new(imagepool, "IMAGE")
    monitor.user_belongings(vmpool_info.xml, "VM")
    monitor.user_belongings(imagepool_info.xml, "IMAGE")
  when "group"
    ## Get total amount
    monitor.get_amount
  when "ha_zone"
    ## Discovery 
    monitor.retrieve_element("#{xml_path}/NAME", discovery_key, "##{key}")
    ##element_info
    monitor.get_zone_health(xml_path)
  when "host"
    ## get_amount_per_state
    monitor.get_amount_per_state("#{xml_path}/STATE",key)
    ##element_info
    monitor.get_resource("#{xml_path}/HOST_SHARE/MAX_CPU", "cluster.total.cpu")
    monitor.get_resource("#{xml_path}/HOST_SHARE/CPU_USAGE", "cluster.allocated.cpu")
    monitor.get_resource("#{xml_path}/HOST_SHARE/USED_CPU", "cluster.used.cpu")
    monitor.get_resource("#{xml_path}/HOST_SHARE/MAX_MEM", "cluster.total.ram")
    monitor.get_resource("#{xml_path}/HOST_SHARE/MEM_USAGE", "cluster.allocated.ram")
    monitor.get_resource("#{xml_path}/HOST_SHARE/USED_MEM", "cluster.used.ram")
    monitor.get_unique_resource_per_cluster(xml_path, "HOST_SHARE/TOTAL_CPU", "cluster.real.cpu")
    monitor.get_unique_resource_per_cluster(xml_path, "HOST_SHARE/TOTAL_MEM", "cluster.real.ram")
  when "vm"
    ## get_amount_per_state
    monitor.get_amount_per_state("#{xml_path}/STATE",key)
    ## get_amount_per_lcm_state 
    monitor.get_amount_per_state("#{xml_path}/LCM_STATE",lcm_key)
    ### element_info
    monitor.vms_info(key, elements)
    ### discovery
    monitor.retrieve_element("#{xml_path}/NAME", discovery_key, "##{key}")
  when "image"
    ## get_amount_per_state
    monitor.get_amount_per_state("#{xml_path}/STATE",key)
    ### element_info
    monitor.images_info(key, elements)
    ### discovery
    monitor.retrieve_element("#{xml_path}/NAME", discovery_key, "##{key}")
  when "cluster"
    ## get total amount
    monitor.get_amount
  when "vnet"
    ## get total amount
    ## TODO: WRONG! This is not get_total_amount. Should be element_info
    monitor.get_ip_amount
  else
    STDERR.puts "Resource not recognized"
  end
else
  STDERR.puts "ERROR. Action not supported."
end


