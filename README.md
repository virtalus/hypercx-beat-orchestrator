# HyperCX Beat

**HyperCX Beat** is Virtalus monitoring solution available in **HyperCX Bento** clusters. This monitoring solution is based on **Zabbix** and it monitors several components in the **HyperCX** stack. Some of those are present in the following repository.

## Orchestrator

**HyperCX** leverages a fork of **OpenNebula** maintained by Virtalus as the cloud orchestrator. This repository contains the code and templates used to monitor this orchestrator on zabbix and it is fully compatible with **HyperCX** and **OpenNebula** clusters. It uses trapper items to send information to the server or a proxy. These templates and script provide accounting, performance and failure monitoring.

Tested environments:
* Zabbix:  >= 4.2
* HyperCX: >= 10.4.2
* OpenNebula: >= 5.8.0

![](../picts/orchestrator-dashboard.png)

### Installation

There are two templates to monitor the orhestrator. *orchestrator/HyperCX-Orchestrator* is always used while *orchestrator/HyperCX-Orchestrator-HA* is also added when HA clusters are used. When uploading the template directly to **Zabbix** you should use the xml version of the template, but when using the API you will need to use the json version.

**orchestrator/sender.rb** is used to generate and send all the metrics. You can place this file in any place, for example, inside */var/lib/one*. It should be owned by **oneadmin** and enable the execution flag. Usage of the script is as follows:

```
Collect metrics from HyperCX
Usage: sender.rb [OPTIONS]
Example
sender.rb -s 172.17.52.5 -o Cluster_Name -r vm -a get_amount_per_state

    -r, --resource STR               Resource to monitor. Available options: vm, host, cluster, group, vnet, datastore, user, ha_zone.
    -a, --action STR                 Monitoring action. Available options: get_total_amount, get_amount_per_state, discovery, element_info.
    -s, --zabbix_server STR          Zabbix server.
    -p, --zabbix_port STR            Zabbix port. Default: 10051.
    -o, --hostname STR               Hostname specified on zabbix for this host
    -v, --verbose

    -h, --help                       Show this help message.
```

Not all monitoring actions are available for all the resources, but this approach attempts to be flexible and allow users to only monitor required elements. It is also very flexible allowing users to easily add more metrics. In order to test any resource or action, the script can be ran using the **-v** flag. This will also help to debug if the data is not received by the zabbix server.

This script must be triggered periodically to send information to Zabbix server. A good approach is to use oneadmin's crontab for this task. The following code lists all the information that can be fetched using this script, and also some recommended intervals:

```
5 * * * * ruby /var/lib/one/sender.rb -s $SERVER_IP -o $HOSTNAME -r vm -a get_amount_per_state
5 * * * * ruby /var/lib/one/sender.rb -s $SERVER_IP -o $HOSTNAME -r datastore -a element_info
5 * * * * ruby /var/lib/one/sender.rb -s $SERVER_IP -o $HOSTNAME -r host -a element_info
5 * * * * ruby /var/lib/one/sender.rb -s $SERVER_IP -o $HOSTNAME -r vm -a get_amount_per_state
5 * * * * ruby /var/lib/one/sender.rb -s $SERVER_IP -o $HOSTNAME -r datastore -a element_info
30 * * * * ruby /var/lib/one/sender.rb -s $SERVER_IP -o $HOSTNAME -r image -a get_amount_per_state
30 * * * * ruby /var/lib/one/sender.rb -s $SERVER_IP -o $HOSTNAME -r user -a element_info
30 * * * * ruby /var/lib/one/sender.rb -s $SERVER_IP -o $HOSTNAME -r host -a get_amount_per_state
30 * * * * ruby /var/lib/one/sender.rb -s $SERVER_IP -o $HOSTNAME -r cluster -a get_total_amount
30 * * * * ruby /var/lib/one/sender.rb -s $SERVER_IP -o $HOSTNAME -r vnet -a get_total_amount
30 * * * * ruby /var/lib/one/sender.rb -s $SERVER_IP -o $HOSTNAME -r group -a get_total_amount
30 * * * * ruby /var/lib/one/sender.rb -s $SERVER_IP -o $HOSTNAME -r datastore -a discovery
30 * * * * ruby /var/lib/one/sender.rb -s $SERVER_IP -o $HOSTNAME -r user -a discovery
```

For HA clusters, the following items should also be configured together with the HA template in zabbix:

```
30 * * * * ruby /var/lib/one/sender.rb -s $SERVER_IP -o $HOSTNAME -r ha_zone -a element_info
30 * * * * ruby /var/lib/one/sender.rb -s $SERVER_IP -o $HOSTNAME -r ha_zone -a discovery
```

